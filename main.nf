#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.input  = ''
params.outdir = ''

Channel
  .fromPath(params.input)
  .splitCsv(header:true)
  .map{ row-> tuple(row.id, file(row.f1), file(row.f2)) }
  .view()
  .set{ input_ch }

process MiXCR {
  publishDir "$params.outdir/$id", mode: 'copy', overwrite: true
  input:
    tuple val(id), path(f1), path(f2)
  output:
    path 'analysis*'
  script:
  """
  mixcr -Xmx1G analyze shotgun \
        --receptor-type tcr \
        --species hs \
        --starting-material rna \
        --only-productive \
        --report analysis.report \
        $f1 $f2 analysis.$id
  """
}

workflow {
  MiXCR(input_ch)
}
